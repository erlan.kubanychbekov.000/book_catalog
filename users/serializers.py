from django.contrib.auth.password_validation import validate_password
from rest_framework import serializers
from .models import User


class RegisterUserSerializer(serializers.ModelSerializer):
    repeat_password = serializers.CharField()

    class Meta:
        model = User
        fields = ('email', 'username', 'password', 'repeat_password')

    def validate_password(self, value):
        """
        Validate the password using Django password validators
        """
        validate_password(value)
        return value

    def create(self, validated_data):
        """
        Create user and check repeat_password == password
        """
        repeat_password = validated_data.pop('repeat_password')
        if repeat_password == validated_data['password']:
            user = User.objects.create_user(**validated_data)
            return user
        else:
            raise serializers.ValidationError("Invalid password")
