from django.db import models
from django.conf import settings
from django.db.models import Avg
from .managers import BookManager


class Genre(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Book(models.Model):
    slug = models.SlugField()
    title = models.CharField(max_length=50)
    genre = models.ManyToManyField(Genre)
    date = models.DateField(auto_now=True)
    description = models.TextField()
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True)
    objects = BookManager()

    def __str__(self):
        return self.title


class Favorite(models.Model):
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
