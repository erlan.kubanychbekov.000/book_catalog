from django.urls import path
from .views import (BookListView, RetrivBookView, AddFavoriteBookView)

app_name = 'books'

urlpatterns = [
    path('', BookListView.as_view(), name='main'),
    path('book/<slug:slug>/', RetrivBookView.as_view(), name='book-detail'),
    path('add/favorite/', AddFavoriteBookView.as_view(), name='add-favorite')
]
