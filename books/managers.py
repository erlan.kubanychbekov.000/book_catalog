from django.db import models
from django.db.models import Avg, BooleanField, Case, When, Value


class BookManager(models.Manager):
    def with_rating_and_favorites(self, user):
        queryset = (
            self.select_related('author')
            .prefetch_related('genre')
            .annotate(
                avg_rating=Avg('review__rating'),
                is_favorite=Case(
                    When(favorite__author=user, then=Value(True)),
                    default=Value(False),
                    output_field=BooleanField()
                )
            )

        )
        return queryset
