from rest_framework.generics import (ListAPIView, RetrieveAPIView, CreateAPIView)
from django_filters.rest_framework import DjangoFilterBackend
from .models import Book, Favorite
from .serializers import (ListBookSerializer, RetrivBookSerializer, FavoriteSerializer)
from .filters import BookFilter


class BookListView(ListAPIView):
    """
    Book List View

    This view provides a list of books and supports filtering by date, genre, and author.

    - `date` (optional): Filter books by publication date.
    - `genre` (optional): Filter books by genre.
    - `author` (optional): Filter books by author.

    """
    serializer_class = ListBookSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_class = BookFilter

    def get_queryset(self):
        queryset = Book.objects.with_rating_and_favorites(self.request.user)
        return queryset


class RetrivBookView(RetrieveAPIView):
    """
    Retrieve Book View

    This view retrieves information about a specific book using its unique slug.

    - `slug`: Unique identifier for the book.

    """
    serializer_class = RetrivBookSerializer
    lookup_field = 'slug'

    def get_queryset(self):
        queryset = Book.objects.with_rating_and_favorites(self.request.user)
        return queryset


class AddFavoriteBookView(CreateAPIView):
    """
    Add to Favorites View

    This view allows users to add a book to their favorites list.

    - `book`: ID of the book to add to favorites.

    """
    queryset = Favorite.objects.all()
    serializer_class = FavoriteSerializer
