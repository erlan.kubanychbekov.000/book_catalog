from django_filters import rest_framework as filters
from .models import Book


class BookFilter(filters.FilterSet):
    author = filters.CharFilter(
        field_name='author__username',
        lookup_expr='icontains')

    class Meta:
        model = Book
        fields = {
            'date': ['gte', 'lte'],
            'genre': ['exact'],
        }
