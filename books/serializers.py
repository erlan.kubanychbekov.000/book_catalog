from rest_framework import serializers
from reviews.serializers import ReviewSerializer
from .models import (Book, Genre, Favorite)


class GenreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Genre
        fields = ('name',)


class ListBookSerializer(serializers.ModelSerializer):
    """
    Serializer for listing books.

    Provides a list of books with details like title, author, genre, average rating, and whether the book is a favorite for the user (if authenticated).
    """
    author = serializers.StringRelatedField()
    url = serializers.HyperlinkedIdentityField(
        view_name='books:book-detail',
        lookup_field='slug',
        read_only=True)
    genre = serializers.StringRelatedField(many=True)
    avg_rating = serializers.DecimalField(max_digits=3, decimal_places=2, read_only=True)
    is_favorite = serializers.BooleanField(read_only=True)

    class Meta:
        model = Book
        fields = ('url', 'title', 'author', 'genre', 'avg_rating', 'is_favorite', 'date')


class RetrivBookSerializer(serializers.ModelSerializer):
    """
    Serializer for retrieving a single book.

    Provides detailed information about a book, including title, author, genre, average rating, date published, description, and reviews.
    """
    reviews = ReviewSerializer(many=True, read_only=True, source='review_set')
    avg_rating = serializers.DecimalField(max_digits=3, decimal_places=2, read_only=True)
    is_favorite = serializers.BooleanField(read_only=True)

    class Meta:
        model = Book
        fields = ('title', 'author', 'genre', 'is_favorite', 'avg_rating', 'date', 'description', 'reviews')


class FavoriteSerializer(serializers.ModelSerializer):
    """
    Serializer for managing user favorites.

    Allows users to add books to their favorites list.
    """

    class Meta:
        model = Favorite
        fields = ('book',)

    def create(self, validated_data):
        user_id = self.context['request'].user.id
        favorite = Favorite.objects.create(**validated_data, author_id=user_id)
        return favorite
