from rest_framework.generics import CreateAPIView
from .models import Review
from .serializers import ReviewSerializer


class AddReviewView(CreateAPIView):
    """
    Add review view.
    -`book`: ID of the book.
    -`rating`: INT.
    -`comment`: TEXT.
    """
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer

