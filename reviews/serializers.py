from rest_framework import serializers
from .models import Review


class ReviewSerializer(serializers.ModelSerializer):
    """
       Serializer for reviews.

       Provides serialization and deserialization for review data including book, rating, and comment.
       Allows users to create reviews associated with a book.
    """

    class Meta:
        model = Review
        fields = ('book', 'rating', 'comment')

    def create(self, validated_data):
        user_id = self.context['request'].user.id
        review = Review.objects.create(**validated_data, user_id=user_id)
        return review
