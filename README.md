## Book catalog ##

To run the project you need `python3.8 >`

Set up virtual environment.

``` commandline
python3 -m venv venv
source venv/bin/activate
```
Install dependencies.
```commandline
pip install -r requierments.txt
```
Create a `.env` file and fill it out.

```text
SECRET_KEY="django-insecure-4tuz#dlc#lo7yecg8e#+^&6q%c^*-2e1m2_i^vd^bvpc5mzulu"
DEBUG=True
EMAIL_HOST_USER=email
EMAIL_HOST_PASSWORD=password
ALLOWED_HOSTS=*
```
Run migrations
```commandline
python manage.py migrate
```
Start server.
```commandline
python manage.py runserver
```
The API will be available via the link http://127.0.0.1:8000/api/

swagger root http://127.0.0.1:8000/swagger